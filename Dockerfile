FROM debian:bookworm-slim

ARG BYOND_MAJOR=514
ARG BYOND_MINOR=1585
ARG RUST_G_VER=1.0.2

ENV DEBIAN_FRONTEND noninteractive

COPY dm-progress /usr/local/bin

RUN echo force-unsafe-io >> /etc/dpkg/dpkg.cfg && \
    dpkg --add-architecture i386 && \
    apt update && \
    apt full-upgrade -yy && \
    apt install -yy --no-install-recommends \
    ca-certificates curl lib32stdc++6 libc6-i386 libssl1.1:i386 make mariadb-client-core-10.6:i386 pv unzip && \
    curl -Lf "https://www.byond.com/download/build/${BYOND_MAJOR}/${BYOND_MAJOR}.${BYOND_MINOR}_byond_linux.zip" > .zip && \
    curl -Lf "https://github.com/tgstation/rust-g/releases/download/${RUST_G_VER}/librust_g.so" > /usr/local/lib/librust_g.so && \
    unzip -j -d /usr/local/bin/ .zip 'byond/bin/*' -x 'byond/bin/*.so' && \
    unzip -j -d /usr/local/lib/ .zip 'byond/bin/*.so' && \
    apt remove -yy curl unzip && \
    apt autoremove -yy --purge && \
    apt clean && \
    ldconfig && \
    rm -rf .zip /var/lib/apt/lists
