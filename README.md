Hewwo :3

This Docker/Buildah/Podman image is intended as a replacement base image for [CHOMPStation's Dockerfile](https://github.com/CHOMPStation2/CHOMPStation2/blob/master/Dockerfile) to hopefully make the CHOMPStation development cycle faster, or at the very least shorten the Dockerfile. Also take a lookie at my proposed [CHOMPStation Dockerfile](https://github.com/Higgs1/CHOMPStation2/blob/lexxy-launch/Dockerfile) too.

Current features:
* Comes with Byond and 32-bit libraries pre-installed
* Comes with [rust_g](https://github.com/tgstation/rust-g)
* Tagged using Byond's version.
* CI builds
* Reduces apt-getting

Future TODO features:
* Include [byhttp](https://github.com/Lohikar/byhttp)
* Build rust_g from sauce
* Build byhttp from sauce
* Include whatever else is used in CHOMPStation development

# Example Usage

```
FROM registry.gitlab.com/lexxyfox/chompstation2-docker:514.1585
```
